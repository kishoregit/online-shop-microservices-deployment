helm install -f values2/redis-values.yaml rediscart charts2/redis

helm install -f values2/email-service-values.yaml emailservice charts2/microservice2
helm install -f values2/cart-service-values.yaml cartservice charts2/microservice2
helm install -f values2/currency-service-values.yaml currencyservice charts2/microservice2
helm install -f values2/payment-service-values.yaml paymentservice charts2/microservice2
helm install -f values2/recommendation-service-values.yaml recommendationservice charts2/microservice2
helm install -f values2/productcatalog-service-values.yaml productcatalogservice charts2/microservice2
helm install -f values2/shipping-service-values.yaml shippingservice charts2/microservice2
helm install -f values2/ad-service-values.yaml adservice charts2/microservice2
helm install -f values2/checkout-service-values.yaml checkoutservice charts2/microservice2
helm install -f values2/frontend-values.yaml frontendservice charts2/microservice2
